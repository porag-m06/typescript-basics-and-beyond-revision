# TypeScript Basics and Beyond Revision

This is about TypeScript (an open-source language that builds on JavaScript). TypeScript is a programming language developed and maintained by Microsoft. It is a strict syntactical superset of JavaScript. Want to revise? Let's get started :)